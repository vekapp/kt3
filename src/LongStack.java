import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LongStack {

   public static void main (String[] argum) {
      String s = "2 5 9 ROT + SWAP -";
      System.out.println(interpret(s));
   }

   private LinkedList<Long> linkedList = new LinkedList<>();

   @Override
   public Object clone() throws CloneNotSupportedException {
      // Tegin samamoodi nagu oli siin https://enos.itcollege.ee/~jpoial/algoritmid/adt.html näidatud!
      LongStack clone = new LongStack();
      if ((linkedList.size() - 1) >= 0)
         for (int i = 0; i <= (linkedList.size() - 1); i++)
            clone.linkedList.add(this.linkedList.get(i));
      return clone;
   }

   public boolean stEmpty() {
      return ((linkedList.size() - 1) < 0);
   }

   public void push (long a) {
      linkedList.addLast(a);
   }

   public long pop() {
      if (stEmpty()) throw new IndexOutOfBoundsException("Stack underflow");
      long number = linkedList.remove(linkedList.size() - 1);
      return number;
   }

   public void op (String s) {
      // Tegin samamoodi nagu oli siin https://enos.itcollege.ee/~jpoial/algoritmid/adt.html näidatud!
      if (!isValidOperator(s)){
         throw new IllegalArgumentException("Invalid operation: " + s);
      }
      if (linkedList.size() < 2)
         throw new IndexOutOfBoundsException("Too few elements for " + s);

      if (s.equals("+")){
         long op2 = pop();
         long op1 = pop();
         push(op1 + op2);
      }
      else if (s.equals("-")){
         long op2 = pop();
         long op1 = pop();
         push(op1 - op2);
      }
      else if (s.equals("*")){
         long op2 = pop();
         long op1 = pop();
         push(op1 * op2);
      }
      else if (s.equals("/")){
         long op2 = pop();
         long op1 = pop();
         push(op1 / op2);
      }
      else if (s.equals("ROT")){
         long op3 = pop();
         long op2 = pop();
         long op1 = pop();
         push(op2);
         push(op3);
         push(op1);
      } else if (s.equals("SWAP")){
         long op2 = pop();
         long op1 = pop();
         push(op2);
         push(op1);
      }
   }


   public long tos() {
      // Tegin samamoodi nagu oli siin https://enos.itcollege.ee/~jpoial/algoritmid/adt.html näidatud!
      if (stEmpty())
         throw new IndexOutOfBoundsException(" stack underflow");
      return linkedList.get(linkedList.size() - 1);
   }

   @Override
   public boolean equals (Object o) {
      // Tegin samamoodi nagu oli siin https://enos.itcollege.ee/~jpoial/algoritmid/adt.html näidatud!
      if (((LongStack) o).linkedList.size() - 1 != linkedList.size() - 1)
         return false;
      for (int i = 0; i <= linkedList.size() - 1; i++)
         if (((LongStack) o).linkedList.get(i) != linkedList.get(i))
            return false;
      return true;
   }

   @Override
   public String toString() {
      return linkedList.toString();
   }

   public static long interpret (String expression) {
      expression = expression.replaceAll("\\s+", " ");
      if (expression.length() == 0){
         throw new RuntimeException("Empty expression");
      }
      LongStack numbers = new LongStack();
      List<String> myList = new ArrayList<>(Arrays.asList(expression.split(" ")));

      for (int i = 0; i < myList.size(); i++) {
         try {
            long number = Long.parseLong(myList.get(i));
            numbers.push(number);
         } catch (NumberFormatException e){
            try {
               String operator = myList.get(i);
               if (!operator.equals("")){
                  numbers.op(operator);
               }
            } catch (IndexOutOfBoundsException e1){
               throw new RuntimeException("Cannot perform " +
                       myList.get(i) +
                       " in expression " +
                       "\"" + expression + "\"");
            } catch (IllegalArgumentException e2){
               throw new RuntimeException("Illegal symbol " +
                       myList.get(i) +
                       " in expression " +
                       "\"" + expression + "\"");
            }
         }
      }
      long result = numbers.pop();
      if (numbers.stEmpty()){
         return result;
      } else {
         throw new RuntimeException("Too many numbers in expression " + "\"" + expression + "\"");
      }
   }

   private boolean isValidOperator(String operator){
      if (operator.equals("+")){
         return true;
      } else if (operator.equals("-")){
         return true;
      } else if (operator.equals("*")){
         return true;
      } else if (operator.equals("SWAP")){
         return true;
      } else if (operator.equals("ROT")){
         return true;
      } else return operator.equals("/");
   }
}